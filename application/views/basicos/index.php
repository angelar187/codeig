<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplos basicos</title>
</head>
<body>
  <h1>
    Ejemplos basicos
  </h1>
	<a class="btn btn-primary" href="<?php echo base_url("/curso"); ?>" role="button">Volver</a>
  <a class="btn btn-primary" href="<?php echo base_url("/curso/basicos/javascript"); ?>" role="button">Javascript</a>
  <a class="btn btn-primary" href="<?php echo base_url("/curso/basicos/jquery"); ?>" role="button">Jquery</a>
  <a class="btn btn-primary" href="<?php echo base_url("/curso/basicos/angular"); ?>" role="button">Angular</a>
</body>
</html>
